# brew-python-rpm
OSX 'sdist' Bindings for python3-rpm package. This package was intended to allow access to [rpmlint](https://github.com/rpm-software-management/rpmlint) on OSX.

## Install
You should be able to `brew install rpm`. This provides the system level API, sans python support.
  
Now to install the Python3.9.x Bindings/Lbraries. ( Uses rpm 4.1.17 as an Example )
```
# xcode-select --install
# git clone https://gitea.com/xltdp/brew-python-rpm.git
# python3 -m pip dist/4.17.0/intel/rpm-4.17.0-py3-none-any.whl
```
You can now verify everything is in place by installing:
```brew install rpmlint```

## Process for updating Bindings
Install rpm ( shocker right? )
```brew install rpm```
Now to edit, patch, or replace this recipe. I've included a sample patch in the src folder [Sample 4.1.17 rpm.rb.patch](src/4.1.17/rpm.rb.patch). You can dry run the patch with the following command.
```
patch src/rpm.rb.orig --dry-run --verbose -p0 < src/rpm.rb.patch 
```
Edit could be achieved by seeing what I replaced from the patch, and replacing the recipe is an option if it doesn't change much. If by chance you mangle your original rpm.rb, you can go to it's 'Formula' directory, and issue a `brew update-reset`( [issues#5108](https://github.com/Homebrew/brew/issues/5108) ). This will reset all your edits on files back to originals.   
  
My current Formula directory is: `/usr/local/Homebrew/Library/Taps/homebrew/homebrew-core/Formula/`  
  
Once you've updated the brew formula, time to compile!  
```brew install --build-from-source --verbose --debug rpm```  
If you've already got it installed for some reason, we can reinstall and compile at the same time  
```brew reinstall --build-from-source --verbose --debug rpm```  

Once your install is completed and compiled ( Woot! ) your rpm binaries are ready. As an added bonus because we enabled python in the build flag we've now got the python3-rpm bindings for python3.9.x . However because we're using Brew, and it installed the python3-rpm libraries to Brew's `/usr/local` structure in `/usr/local/lib/python3.9/site-packages/` we need to package those so we can install them into OSXs sDist Python 3.9.x install. 
  
Building a quick wheel package is easy by copying `/usr/local/Cellar/rpm/<your_version>/lib/python3.<your_version>/site-packages/rpm`, [setup.py](src/setup.py), [MANIFEST.in](src/MANIFEST.in) into `~/scratch/`, move into `~/scratch/`, and build wheels with the following command:
``` 
~/scratch/$ python3 setup.py bdist_wheel
```
To verify the contents of your build with `unzip -l dist/*`  
```
Archive:  dist/rpm-4.17.0-py3-none-any.whl
  Length      Date    Time    Name
---------  ---------- -----   ----
     3070  12-09-2022 21:17   rpm/__init__.py
   169424  12-09-2022 21:17   rpm/_rpm.so
     5922  12-09-2022 21:17   rpm/transaction.py
      237  12-10-2022 03:09   rpm-4.17.0.dist-info/METADATA
       92  12-10-2022 03:09   rpm-4.17.0.dist-info/WHEEL
        4  12-10-2022 03:09   rpm-4.17.0.dist-info/top_level.txt
      501  12-10-2022 03:09   rpm-4.17.0.dist-info/RECORD
---------                     -------
   179250                     7 files
```
We've now repackaged those bindings into a wheel, lets go ahead and install it in the systems Python3.9.
```
~/scratch/$ /usr/bin/python3 -m pip install dist/rpm-4.17.0-py3-none-any.whl
Defaulting to user installation because normal site-packages is not writeable
Processing ./dist/rpm-4.17.0-py3-none-any.whl
Installing collected packages: rpm
Successfully installed rpm-4.17.0
~/scratch/$ /usr/bin/python3 -m pip freeze | grep rpm
rpm @ file:///usr/local/lib/python3.9/site-packages/rpm/dist/rpm-4.17.0-py3-none-any.whl
```
## References
http://jeromebelleman.gitlab.io/posts/devops/setuppy/  
https://realpython.com/python-wheels/  
https://docs.moodle.org/dev/How_to_create_a_patch  
https://docs.moodle.org/dev/How_to_apply_a_patch  
https://python-packaging.readthedocs.io/en/latest/non-code-files.html  

## Gotchas
Fixing `readelf: command not found`
```
$ rpmlint ~/Downloads/my_test_rpm.rpm 
/bin/sh: readelf: command not found
(none): E: fatal error while reading /Users/<your_user>/Downloads/my_test_rpm.rpm : Command 'readelf --help' returned non-zero exit status 127.
```
You'll need to install `binutils` & Add it's Path:
```
$ brew install binutils

...

==> Caveats
binutils is keg-only, which means it was not symlinked into /usr/local,
because it shadows the host toolchain.

If you need to have binutils first in your PATH, run:
  echo 'export PATH="/usr/local/opt/binutils/bin:$PATH"' >> /Users/lcurran/.bash_profile

For compilers to find binutils you may need to set:
  export LDFLAGS="-L/usr/local/opt/binutils/lib"
  export CPPFLAGS="-I/usr/local/opt/binutils/include"

==> Summary
🍺  /usr/local/Cellar/binutils/2.39_1: 83 files, 162.7MB
```
