from setuptools import setup
# https://python-packaging.readthedocs.io/en/latest/non-code-files.html
setup(
    name='rpm',
    description='RPM Python3 sdist Bindings',
    url='https://gitea.com/xltdp/brew-python-rpm',
    author='Llewellyn Curran',
    author_email='melinko2003@gmail.com',
    version='4.17.0',
    license='MIT', 
    packages=['rpm'], 
    include_package_data=True,
)
